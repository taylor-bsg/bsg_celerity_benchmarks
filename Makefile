include ./mk/Makefile.user.config
include ./mk/Makefile.setup
include ./mk/Makefile.repo
include ./mk/Makefile.func

.PHONY: checkout-repos

checkout-repos:
	$(call checkout_baseline_func,$(BSG_REPOS),$(VCS-BUILD-DIR))

build-manycore-tools:
	if [ ! -d $(MANYCORE_TOOLS_INSTALL_DIR) ];then       						\
		cd $(MANYCORE_TOOLS_BUILD_DIR) && make checkout-all && make build-riscv-tools && cd -;	\
		cd $(VCS-BUILD-DIR)/bsg_manycore && git submodule update --init imports/vscale;			\
	fi

build-rocket-tools:
	cd $(ROCKET_TOOLS_BUILD_DIR) && make  checkout-all
	cd $(ROCKET_TOOLS_BUILD_DIR) && make  build-riscv-tools-newlib

loopback-test:
	make -C $(ROCKET_RUN_DIR) run BENCHMARK_0=bsg_rocket_loopback
